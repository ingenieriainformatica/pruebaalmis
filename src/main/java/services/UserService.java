package services;

import java.util.ArrayList;
import model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.UserRepository;

@Service
public class UserService {
  @Autowired
  UserRepository userRepository;
  
  public ArrayList <UserModel> getUsers() {
    return (ArrayList <UserModel>)userRepository.findAll();
  }
}
