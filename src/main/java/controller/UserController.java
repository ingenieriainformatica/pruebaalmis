package controller;

import java.util.ArrayList;
import model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import services.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
  @Autowired
  UserService userService;
  
  @GetMapping()
  public ArrayList<UserModel> getUsers() {
    return userService.getUsers();
  }
  
}
